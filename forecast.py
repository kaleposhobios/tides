import maya


class Forecast:

    def __init__(self, tide_html):
        self.tide_html = tide_html

    def render(self):
        print("Date: {}".format(self.display_date()))
        for low_tide in self.low_tides():
            print("Time: {}".format(low_tide['time']))
            print("Height: {}".format(low_tide['height']))

    def display_date(self):
        text = self.tide_html.select_one(".tide-day__date").text
        return text.split(":")[1].strip()

    def low_tides(self):
        tide_rows = self.tide_html.find_all("tr")
        daytime_low_tide_rows = filter(self.is_daytime_low_tide, tide_rows)
        return map(self.tide_details, daytime_low_tide_rows)

    def is_daytime_low_tide(self, tide_row):
        if "Low Tide" in tide_row.text and self.is_daytime(tide_row):
            return True
        else:
            return False

    def is_daytime(self, tide_row):
        sunrise_time = self.get_sunrise()
        sunset_time = self.get_sunset()
        tide_time = self.get_tide_time(tide_row)
        return sunrise_time < tide_time < sunset_time

    def get_sunrise(self):
        display_time = self.tide_html.select(".tide-day__sun-moon-cell")[0].text.replace("Sunrise: ", "")
        return maya.parse(display_time)

    def get_sunset(self):
        display_time = self.tide_html.select(".tide-day__sun-moon-cell")[1].text.replace("Sunset: ", "")
        return maya.parse(display_time)

    def get_tide_time(self, tide_row):
        return maya.parse(tide_row.b.text)

    def get_tide_height(self, tide_row):
        return tide_row.select_one(".js-two-units-length-value__primary").text

    def tide_details(self, tide_row):
        return {
            "time": self.get_tide_time(tide_row).datetime().strftime("%H:%M:%S %p"),
            "height": self.get_tide_height(tide_row)
        }
