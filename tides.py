from bs4 import BeautifulSoup
from forecast import Forecast
import requests


def print_tides():
    locations = get_all_forecasts()
    for location in locations:
        print(location['name'])
        for forecast in location['forecasts']:
            print("***")
            forecast.render()
        print("**************")


def get_all_forecasts():
    return map(
        get_forecast, [
            {
                "url": "https://www.tide-forecast.com/locations/Half-Moon-Bay-California/tides/latest",
                "name": "Half Moon Bay, California"
            },
            {
                "url": "https://www.tide-forecast.com/locations/Huntington-Beach/tides/latest",
                "name": "Huntington Beach, California"
            },
            {
                "url": "https://www.tide-forecast.com/locations/Providence-Rhode-Island/tides/latest",
                "name": "Providence, Rhode Island"
            },
            {
                "url": "https://www.tide-forecast.com/locations/Wrightsville-Beach-North-Carolina/tides/latest",
                "name": "Wrightsville Beach, North Carolina"
            }
        ]
    )


def get_forecast(location):
    r = requests.get(location["url"])
    html = r.text
    content = BeautifulSoup(html, "html.parser")
    tide_days = content.find_all("div", {"class": "tide-day"})
    return {
            "name": location["name"],
            "forecasts": map(Forecast, tide_days)
        }


if __name__ == '__main__':
    print_tides()

