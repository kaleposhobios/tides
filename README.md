# Matt's Tides Code Sample

## Setup
pip3 install requests

pip3 install maya

pip3 install bs4

## Running
python3 tides.py

## Output
As requested, daytime low tides for 4 locations.

## Notes
It would be fun to discuss all the tradeoffs here.
I was focused on solving the problem with sane,
reasonable code. It has also been a few years
since I've written Python. I know it will come back
quickly, but in the meantime please forgive any
silly mistakes and the lack of setup.py script.
